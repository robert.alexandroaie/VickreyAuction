
package ac.dc.masma.lab.model.agent.behaviour;

import ac.dc.masma.lab.model.agent.AuctionManager;
import jade.core.AID;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;

public class ManagerSend extends TickerBehaviour {
    
    private AuctionManager myAgent;

        public ManagerSend(AuctionManager a, long period)
        {
            super(a, period);
            myAgent = a;
        }

        @Override
        public void onTick()
        {
            int nParticipants = myAgent.participantList.size();

            if (nParticipants > 1) // still more than 1 agent making offers
            {
                /* New auction round:
                 * send calls to the participants
                   inform them on current auction price
                   ask them to send new offers
                 */
                ACLMessage toSend = new ACLMessage(ACLMessage.INFORM);
                toSend.setContent("current_price" + myAgent.currentPrice);

                for (AID a : myAgent.participantList) {
                    toSend.addReceiver(a);
                }
                
                myAgent.send(toSend);

            }
            else if (nParticipants == 1) //only one agent left, the winner
            {   
                AID winner = myAgent.currentWinner;
                this.stop();
            }
            else // no participants left
            {
                this.stop();
            }
        }
}
