package ac.dc.masma.lab.model.agent.behaviour;

import ac.dc.masma.lab.model.agent.BuyerAgent;
import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;

public class BuyerRegister extends OneShotBehaviour {

    private BuyerAgent myAgent;

    public BuyerRegister(BuyerAgent a) {
        super(a);
        myAgent = a;
    }

    @Override
    public void action() {
        //inform managerAgent that I want to register for the auction

        ACLMessage m = new ACLMessage(ACLMessage.INFORM);

        AID receiverAID = new AID("managerAgent", AID.ISLOCALNAME);
        m.addReceiver(receiverAID);
        m.setContent("registering");
        myAgent.send(m);

//            myAgent.windowsForm.AddTextLine("Registering for auction ...");
    }
}
