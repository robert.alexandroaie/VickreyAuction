package ac.dc.masma.lab.model.agent.behaviour;

import ac.dc.masma.lab.model.agent.BuyerAgent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import java.util.Random;

public class BuyerReceive extends CyclicBehaviour {

    private BuyerAgent myAgent;

    public BuyerReceive(BuyerAgent a) {
        super(a);
        myAgent = a;
    }

    @Override
    public void action() {
        ACLMessage m = myAgent.receive();

        if (m != null) {
            String received = m.getContent();

            if (received.contains("registered")) {
                //manager registered me for the auction
//                myAgent.windowsForm.AddTextLine("Successfully registered for auction");
            }

            if (received.contains("current_price")) {
                // received new call from managerAgent

                //the highest current price is:
                int currentPrice = Integer.parseInt(received.substring(13));
//                myAgent.windowsForm.AddTextLine("Received current price: " + currentPrice);

                // probability to make offer decreases with each price increase
                myAgent.probabilityToBid *= myAgent.probabilityDecreaseFactor;

                ACLMessage toSend = new ACLMessage(ACLMessage.INFORM);
                toSend.addReceiver(m.getSender());
                //use agent name to generate unique seed for random numbers
                int seed = 1573 * Integer.parseInt(myAgent.getLocalName().substring(5));
                //decide if to send new offer or retreat
                if (myAgent.probabilityToBid > new Random(seed).nextDouble()) {
                    //make an offer which is higher than the current price
                    seed = 1723 * Integer.parseInt(myAgent.getLocalName().substring(5));
                    int newPrice = currentPrice + new Random(seed).nextInt(myAgent.maxOfferIncrease - 1) + 1;
                    toSend.setContent("offer" + newPrice);
//                    myAgent.windowsForm.AddTextLine("Sent new offer: " + newPrice);
                } else {
                    //price is too high, retreat from auction
                    toSend.setContent("retreat");
//                    myAgent.windowsForm.AddTextLine("Retreated from auction.");
                }
                myAgent.send(toSend);
            }
        } else {
            block();
        }
    }
}
