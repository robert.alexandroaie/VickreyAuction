
package ac.dc.masma.lab.model.agent.behaviour;

import ac.dc.masma.lab.model.agent.AuctionManager;
import jade.core.AID;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;


public class ManagerReceive extends CyclicBehaviour {
    
    private AuctionManager myAgent;

        public ManagerReceive(AuctionManager a)
        {
            super(a);
            myAgent = a;
        }
        
        @Override
        public void action()
        {
            ACLMessage m = myAgent.receive();

            if (m != null)
            {
                String received = m.getContent();
                AID sender = m.getSender();
                
                if(received.contains("registering"))
                {
                    //sender wants to register for auction
                    myAgent.participantList.add(sender);
                    
                    //reply to sender that registration was successful
                    ACLMessage reply = new ACLMessage(ACLMessage.INFORM);
                    reply.setContent("registered");
                    reply.addReceiver(sender);
                    myAgent.send(reply);

                }

                if (received.contains("offer"))
                { 
                    //received new offer from agent
                    int receivedPrice = Integer.parseInt(received.substring(5));

                    if (receivedPrice > myAgent.currentPrice)
                    {
                        myAgent.currentPrice = receivedPrice;
                        myAgent.currentWinner = sender;
                    }
                }

                if (received.contains("retreat"))
                {
                    //agent retreated
                    myAgent.participantList.remove(sender);
                }
            }
            else {
                block();
            }

        }
}
