package ac.dc.masma.lab.model.agent;

import ac.dc.masma.lab.model.agent.behaviour.ManagerReceive;
import ac.dc.masma.lab.model.agent.behaviour.ManagerSend;
import ac.dc.masma.lab.helper.YellowPages;
import jade.core.AID;
import jade.core.Agent;
import jade.domain.FIPAException;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AuctionManager extends Agent {

    public List<AID> participantList;
    public int currentPrice = 0;
    public AID currentWinner = null;
    int maxStartingPrice = 100;
    int auctionCallInterval = 5000;

    @Override
    public void setup() {
        
        participantList = new LinkedList<>();
        currentPrice = (new Random()).nextInt(maxStartingPrice);

        try {
            YellowPages.registerService("AuctionService", this);
        } catch (FIPAException ex) {
            Logger.getLogger(AuctionManager.class.getName()).log(Level.SEVERE, null, ex);
        }

        addBehaviour(new ManagerReceive(this));
        addBehaviour(new ManagerSend(this, auctionCallInterval));
    }
}
