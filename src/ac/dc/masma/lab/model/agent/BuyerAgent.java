package ac.dc.masma.lab.model.agent;

import ac.dc.masma.lab.helper.YellowPages;
import ac.dc.masma.lab.model.agent.behaviour.BuyerReceive;
import ac.dc.masma.lab.model.agent.behaviour.BuyerRegister;
import jade.core.AID;
import jade.core.Agent;
import jade.domain.FIPAException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BuyerAgent extends Agent {

    public AID providerAID = null;
    public double probabilityToBid = 1.0;
    public double probabilityDecreaseFactor = 0.9;
    public int maxOfferIncrease = 50;

    @Override
    public void setup() {
        try {
            providerAID = YellowPages.findService("AuctionService", this, 10);
        } catch (FIPAException ex) {
            Logger.getLogger(BuyerAgent.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (providerAID == null) {
//            windowsForm.AddTextLine("No auction provider found.");
        } else {
//            windowsForm.AddTextLine("Found auction provider: " + providerAID.getLocalName());

            addBehaviour(new BuyerRegister(this));
            addBehaviour(new BuyerReceive(this));
        }
    }
}
